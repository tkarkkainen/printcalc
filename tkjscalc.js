function CalculationObject(){
	this.description = "";
	this.operation = null;
	this.value = null;
	this.operationValue = null;
	this.displayValue = null;
	this.displayOp = null;

	this.UpdateWithCommand = function(command){
		// Is it a math operation?
		var firstChar = command.charAt(0);
		var lastChar = command.charAt(command.length-1);
		var opchars = "+-/*";
		var isOp = opchars.indexOf(firstChar) > -1;
		var isPerc = lastChar == '%';
		var lastInd = isPerc ? command.length - 1 : command.length;
		var numStr = command.substring(1,lastInd);
		if((isNumeric(numStr) && isOp) || command == "=")
		{
			// Yes! It's a math operation!
			// Do we have room for it?
			if(this.operationValue != null || this.operation != null || this.value != null)
				return false;
			if(command == "=")
			{
				this.operation = "=";
			} else {
				this.operationValue = Number(numStr);
				this.operation = firstChar;
				if(isPerc)
					this.operation += "%";
			}

			return true;
		}

		// is it a simple numeric value?
		if(isNumeric(command))
		{
			if(this.operationValue == null && this.value == null)
			{
				this.value = Number(command);
				this.displayValue = command;
				return true;
			} else {
				// We're saving a number, but this object already has a number!
				return false;
			}
		}

		if(this.description == "" && this.operationValue == null && this.value == null)
		{
			// It's a description and we have room for one!
			this.description = command;
			return true;
		} else {
			// No room for a description!
			if(this.operationValue == null)
				this.operationValue = 0;
			if(this.value == null)
				this.value = 0;
			return false;
		}

	};

	this.CalculateDisplayValue = function(previousValue) {
		previousValue = Number(previousValue);
		switch(this.operation) {
			case "+":
				this.value = previousValue + this.operationValue;
				this.displayValue = "+ " + this.operationValue;
				break;
			case "-":
				this.value = previousValue - this.operationValue;
				this.displayValue = "- " + this.operationValue;
				break;
			case "*":
				this.value = previousValue * this.operationValue;
				this.displayValue = "x " + this.operationValue;
				break;
			case "/":
				this.value = previousValue / this.operationValue;
				this.displayValue = "/ " + this.operationValue;
				break;
			case "+%":
				this.value = previousValue * (1+this.operationValue/100);
				this.displayOp = "+ " + this.operationValue + "%";
				this.displayValue = "+ " + previousValue * this.operationValue/100;
				break;
			case "-%":
				this.value = previousValue * (1-this.operationValue/100);
				this.displayOp = "- " + this.operationValue + "%";
				this.displayValue = "- " + previousValue * this.operationValue/100;
				break;
			case "=":
				this.value = previousValue;
				this.displayValue = "= " + this.value;
		}
		return this.value;
	};

	this.DisplayHTML = function() {
		var outStr = "";
		outStr += "<tr>";
		if(this.displayOp == null && this.displayValue == null)
		{
			outStr += "<td colspan = \"3\" class=\"lonedescription\">" + this.description + "</td>";
		} else if (this.displayOp == null) {
			outStr += "<td colspan = \"2\">" + this.description + "</td>";
		} else {
			outStr += "<td>"+this.description+"</td>";
		}
		if(this.displayOp != null)
		{
			outStr += "<td>"+this.displayOp+"</td>";
		}
		if(this.displayValue != null)
		{
			outStr += "<td>"+this.displayValue+"</td>";
		}
		
		outStr += "</tr>";

		return outStr;
	};
}

function InitJSCalc(){
	PrintStrip = document.getElementById("printstrip");
	CommandField = document.getElementById("command");

	CommandField.onkeypress = function(event){ProcessCommand(event);};

	Items = {
		list: [],		// this list contains the actual items
		currentColumn: 0,
	};
}

function ProcessCommand(event){
	// We're only interested in the ENTER key
	if((event.which || event.keyCode) == 13)
	{
		if(CommandField.value=="")
			CommandField.value = "--- --- --- --- ---";
		AddCommandToList();
		CommandField.value = "";
	}
}

function AddCommandToList(){
	var addingNewRow = false;
	var currentRow = null;
	if(Items.list.length < 1)
	{
		currentRow = new CalculationObject();
		addingNewRow = true;
	} else {
		currentRow = Items.list[Items.list.length-1];
	}

	if(!currentRow.UpdateWithCommand(CommandField.value))
	{
		// we have to create a new one anyway...
		addingNewRow = true;
		currentRow = new CalculationObject();
		currentRow.UpdateWithCommand(CommandField.value);
	}
	
	if(addingNewRow)
	{
		Items.list.push(currentRow);
	}

	UpdatePrintStrip();
}

function UpdatePrintStrip()
{
	var n = Items.list.length;
	var operation = null;
	var value = null;
	var tableStr = "";
	var previousValue = 0;
	for (var i=0; i < n; i++)
	{
		newValue = Items.list[i].CalculateDisplayValue(previousValue);
		tableStr += Items.list[i].DisplayHTML();
		previousValue = newValue;
	}
	PrintStrip.innerHTML = tableStr;
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}